(* Modulen Graph, fil graph.ml *)

(*
 * Denna modul används för att representera en graf. Enligt denna implementation består
 * en graf av en lista med noder. Varje nod håller sedan koll på sina individuella kopplingar
 * (neighbours).
 * 
 * Modulen har funktionalitet för att returnera vandringar (se modulen Path) i grafen. En
 * vandring här kan antingen vara en vandring (walk), väg (trail), stig (path) eller cykel (cycle).
 * Dessa definieras enligt:
 *
 * Vandring - Den generella typen.
 * Väg - Vandring som inte passerar samma kant mer än en gång.
 * Stig - En väg som inte passerar samma hörn mer än en gång.
 * Cykel - En stig som börjar och slutar i samma hörn.
 * 
 * 2019-06-09 Max Jonsson
 *)

module Graph : sig
  type t = Graph of Node.t list

  val empty : t

  val add_edge : Edge.t -> t -> t

  val num_of_nodes : t -> int
  val num_of_edges : t -> int

  val num_trails : t -> int
  val num_paths : t -> int
  val num_cycles : t -> int

  val min_trail : t -> Node.t list
  val min_path : t -> Node.t list
  val min_cycle : t -> Node.t list

  val max_trail : t -> Node.t list
  val max_path : t -> Node.t list
  val max_cycle : t -> Node.t list

  val node_degree_out : t -> (string * int) list

  val print_graph : t -> unit
  
end = struct 
  type t = Graph of Node.t list
        
  let empty = Graph []

  let nodes graph = match graph with
    | Graph lst -> lst

  let make lst = Graph lst

  let find name graph =
    let rec aux name lst =
      match lst with
      | [] -> None
      | h::t -> if name = Node.name h then Some h else aux name t
    in aux name (nodes graph)

  let rec exists_edge start stop lst =
    match lst with
    | [] -> false
    | h::[] -> false
    | h1::(h2::_ as t1) ->                                  
      if ((Node.name h1) = start) && ((Node.name h2) = stop)
      then true else exists_edge start stop t1

  (* Lägger till noden i grafen *)
  let add_node node graph = make (node :: (nodes graph))

  (* Lägger till noden i grafen ENDAST om nod med samma namn inte redan finns *)
  let add_node2 node graph =
    let node_opt = find (Node.name node) graph in
    match node_opt with
    | None -> add_node node graph
    | Some node -> graph

  (* Filtrerar bort alla element som matchar predikatet f *)
  let filter f graph =
    let rec aux func lst =
      match lst with
      | [] -> []
      | h::t -> if f h then aux f t else h :: (aux f t)
    in nodes graph |> aux f |> make

  (* Ersätter nod med samma namn med den som skickas in *)
  let replace graph node =
    let p n = (Node.name node) = (Node.name n) in
    filter p graph |> add_node node

  let add_edge edge graph =
    let start = Edge.get_start edge in
    let stop = Edge.get_end edge in
    let graph = add_node2 stop graph in
    match find (Node.name start) graph with
    | None -> add_node (Node.add_edge start stop) graph
    | Some a -> Node.neighbours a |> List.cons stop
                |> Node.of_list (Node.name start) |> replace graph

  
  (* Operationer på en graf *)

  let num_of_nodes graph = List.length (nodes graph)

  let num_of_edges graph =
    let rec aux nodes =
      match nodes with
      | [] -> 0
      | h::t -> (Node.out_degree h) + (aux t)
    in aux (nodes graph)

  let node_degree_out graph =
    let rec aux nodes =
      match nodes with
      | [] -> []
      | h::t -> (Node.name h, Node.out_degree h) :: aux t
    in nodes graph |> aux

  let print_graph graph =
    List.iter (
      fun a -> Printf.printf "Node %s is connected to%s\n" (Node.name a)
          (List.fold_right (fun elt acc -> acc ^ " " ^ (Node.name elt)) (Node.neighbours a) "")
    ) (nodes graph)

  
  (* Vandringar *)
      
  (* Returnerar alla vandringar som matchar predikatet check_walk *)
  (* För predikatet används någon av valid-funktionerna för väg, stig eller cykel *)
  let all_walks check_walk graph =
    let rec aux queue acc_paths =
      match queue with
      | [] -> acc_paths
      | path::queue ->
        let acc_paths =
          if (List.length (Path.nodes path)) > 1 then path :: acc_paths else acc_paths in
        let new_paths = Path.expand path (nodes graph) in
        let (okpaths, nopaths) = List.partition check_walk new_paths in
        let queue = List.append queue okpaths in
        aux queue acc_paths
    in
    aux (List.map Path.make (nodes graph)) []
      
  let max_walk lst =
    List.fold_right (
      fun path acc -> if (Path.length path) > (Path.length acc) then path else acc
    ) (List.tl lst) (List.hd lst)

  let min_walk lst =
    List.fold_right (
      fun path acc -> if (Path.length path) < (Path.length acc) then path else acc
    ) (List.tl lst) (List.hd lst)


  (* Vägar *)
      
  let valid_trail walk =
    let lst = Path.nodes walk in
    match lst with
    | [] -> false
    | h1::[] -> true
    | h1::(h2::t) -> not (exists_edge (Node.name h1) (Node.name h2) t)

  let all_trails graph = all_walks valid_trail graph
  let max_trail graph = graph |> all_trails |> max_walk |> Path.nodes
  let min_trail graph = graph |> all_trails |> min_walk |> Path.nodes
  let num_trails graph = all_trails graph |> List.length
      

  (* Cykler *)

  let exists_duplicates_cycle trail =
    let rec aux l =
      match l with
      | [] -> false
      | h::t ->
        let (removed, lst) = List.partition (fun a -> a = h) t in
        match removed with
        | [] -> aux lst
        | _ -> true
    in
    let first = Path.first trail in
    let l = List.map (fun node -> Node.name node) (Path.nodes trail) in
    let (removed, lst) = List.partition (fun a -> a = (Node.name first)) l in
    let num_removed = List.length removed in  
    if (num_removed > 2) then true else aux lst

  let valid_cycle trail =
    if (Path.nodes trail |> List.length) = 1 then false else
    if Node.name (Path.first trail) = Node.name (Path.last trail)
    then not (exists_duplicates_cycle trail)
    else false

  (* Använder sig av modulen Spath för att skapa ett set av cykler *)
  (* Nyckeln kommer att vara cykeln hash-värde, vilket gör att dubletter sållas bort *)
  let all_cycles graph =
    let cycles = all_trails graph |> List.filter valid_cycle in
    let datas =
      let rec aux c acc =
        (Spath.PathData.create (Path.hash_cycle c) c) :: acc
      in
      List.fold_right aux cycles []
    in
    let set = Spath.DataSet.of_list datas in
    let filt_datas = Spath.DataSet.elements set in
    let cycles = List.map Spath.PathData.get_data filt_datas in
    cycles

  let max_cycle graph = graph |> all_cycles |> max_walk |> Path.nodes
  let min_cycle graph = graph |> all_cycles |> min_walk |> Path.nodes
  let num_cycles graph = all_cycles graph |> List.length


  (* Stigar *)
      
  let exists_duplicates_path trail =
    let lst = List.map (fun node -> Node.name node) (Path.nodes trail) in
    let rec aux l acc =
      match l with
      | [] -> false
      | h::t -> if List.exists (fun a -> a = h) acc
        then true
        else h::acc |> aux t
    in aux lst []

  let valid_path trail =
    if valid_cycle trail then true
    else not (exists_duplicates_path trail)

  let all_paths graph = all_trails graph |> List.filter valid_path
  let max_path graph = graph |> all_paths |> max_walk |> Path.nodes
  let min_path graph = graph |> all_paths |> min_walk |> Path.nodes
  let num_paths graph = all_paths graph |> List.length

end

include Graph
