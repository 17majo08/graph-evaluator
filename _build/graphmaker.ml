(* Module Graphmaker, file graphmaker.ml *)

(*
 * Denna modul används för att skapa en graf utifrån en textfil med kanter.
 *
 * 2019-06-09 Max Jonsson
 *)

module GraphMaker : sig
  val make_graph : string -> Graph.t
  
end = struct
  exception InvalidEdge of string
  
  let stream_of_name fil =
    let infil = open_in fil in
    try
      Either.right (Stream.from (fun _ ->
          try
            Some (input_line infil)
          with e -> None))
    with e -> close_in infil; Either.throw e

  let edge_of_string str =
    let lst = String.split_on_char '-' (String.trim str) in
    match lst with
    | start :: (stop :: []) -> Either.right (Edge.make start stop)
    | _ -> Either.left (InvalidEdge "Invalid edge format")
  
  let edges_of_stream stream =
    let rec aux stream =
      match Stream.peek stream with
      | None -> []
      | Some _ -> let line = Stream.next stream in
        edge_of_string line :: aux stream
    in
    match stream with
    | Either.Left e -> Either.throw e
    | Either.Right s -> Either.right (aux s)

  let graph_of_edges edges =
    let rec aux lst =
      match lst with
      | [] -> Graph.empty
      | h::t -> match h with
        | Either.Left e -> raise e
        | Either.Right a -> aux t |> Graph.add_edge a
    in
    match edges with
    | Either.Left e -> Either.throw e
    | Either.Right l -> Either.right (aux l)

  let make_graph fil =
  let graf = fil |> stream_of_name |> edges_of_stream |> graph_of_edges in
  match graf with
  | Either.Left e -> raise e
  | Either.Right g -> g

end

include GraphMaker
