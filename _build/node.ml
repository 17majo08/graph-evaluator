(* Modulen Node, fil node.ml *)

(*
 * Denna modul används för att representera en nod. I denna implementation består varje nod
 * av ett namn, samt en lista av nodens grannar. 
 *
 * 2019-06-09 Max Jonsson
 *)

module Node : sig
  type t = Node of (string * t list)

  val make : string -> t
  val of_list : string -> t list -> t

  val name : t -> string
  val add_edge : t -> t -> t
    
  val neighbours : t -> t list
  val out_degree : t -> int
    
end = struct
  type t = Node of (string * t list)

  let make str = Node (str, [])

  let of_list str lst = Node (str, lst)

  let name node = match node with
    | Node (s, _) -> s

  let neighbours node = match node with
    | Node (_, l) -> l

  let add_edge start stop = stop :: neighbours start |> of_list (name start) 

  let out_degree node = List.length (neighbours node)

end

include Node
