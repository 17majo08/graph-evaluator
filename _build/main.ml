(* file main.ml *)

(*
 * Detta är main-filen som initierar programmet. Den tar emot ett antal växlar (listade nedan)
 * och utför de operationer som användaren vill utföra. För att fungera som tänkt krävs att en
 * fil med kanter skickas in med växeln -f. Programmet använder sedan modulen Graphmaker för att
 * skapa grafen, samt modulen Switches för att köra de satta växlarna.
 *
 * Exempelkörning:
 *
 * ocamlbuild -cflags -annot main.byte
 * ./main.byte -f kanter.txt -p
 *
 * 2019-06-09 Max Jonsson
 *)

let nodes = ref false
let edges = ref false
let node_degrees = ref false
    
let num_trails = ref false
let num_paths = ref false
let num_cycles = ref false
    
let min_trail = ref false
let min_path = ref false
let min_cycle = ref false
    
let max_trail = ref false
let max_path = ref false
let max_cycle = ref false
    
let print_graph = ref false
    
let file = ref ""
let rest = ref ""

let () =
  let spec = [("-f", Arg.Set_string file, "\tSet the name of the file containing the edges");
              ("-p", Arg.Set print_graph, "\tPrint the graph");
              ("-n", Arg.Set nodes, "\tPrint number of nodes");
              ("-e", Arg.Set edges, "\tPrint number of edges");
              ("-d", Arg.Set node_degrees, "\tPrint degree of all nodes");
              ("-numt", Arg.Set num_trails, "\tPrint number of trails");
              ("-nump", Arg.Set num_paths, "\tPrint number of paths");
              ("-numc", Arg.Set num_cycles, "\tPrint number of cycles");
              ("-mint", Arg.Set min_trail, "\tPrint shortest of trail");
              ("-minp", Arg.Set min_path, "\tPrint shortest of path");
              ("-minc", Arg.Set min_cycle, "\tPrint shortest of cycle");
              ("-maxt", Arg.Set max_trail, "\tPrint longest of trail");
              ("-maxp", Arg.Set max_path, "\tPrint longest of path");
              ("-maxc", Arg.Set max_cycle, "\tPrint longest of cycle");
              ("--", Arg.Rest (fun s -> rest := !rest ^ s ^ " "),
               "\tRest of arguments are stored");]
  in
  Arg.parse
    (Arg.align spec)
    (fun str -> file := str)
    (
      "This program constructs a Graph from a file of edges.\n" ^
      "The file sent in should contain edges on the form \"A-B\".\n" ^
      "Each edge must start on a new row and no whitespaces are allowed.\n"
    )

let () =
  let graf = Graphmaker.make_graph !file in
  if !print_graph then Switches.print_graph graf;
  if !nodes then Switches.nodes graf;
  if !edges then Switches.edges graf;
  if !node_degrees then Switches.degrees graf;
  if !num_trails then Switches.num_trails graf;
  if !num_paths then Switches.num_paths graf;
  if !num_cycles then Switches.num_cycles graf;
  if !min_trail then Switches.min_trail graf;
  if !min_path then Switches.min_path graf;
  if !min_cycle then Switches.min_cycle graf;
  if !max_trail then Switches.max_trail graf;
  if !max_path then Switches.max_path graf;
  if !max_cycle then Switches.max_cycle graf
