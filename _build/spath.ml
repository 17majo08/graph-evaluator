(*
 * Detta är ett program som demonstrerar hur man kan skapa Set (mängder)
 * från Ocamls module Set.
 *
 * Här sätter jag att datat lagras via modul-funktorn Data, som
 * tar en modul som sätter vad datat skall lagras som. Den har 
 * datatypen t och funktionen compare som Set använder sig av för
 * att kolla dubletter.
 *
 * Den här implementationen använder fältet key för att jämföra datat
 * och har därför en egen compare som bara jämför fältet key.
 * 
 * Använder man standardmoduler, så jämför de all data, exempelvis med
 * Set.Make(String) etc.
 *
 * 20190528 Anders Jackson - Testprogram för standardmodulen Set.
 *)

(* En funktor som genererar en modul för att manipulera data *)
module Data(D: sig type elt end) = struct
  type key = string
  (* Det data som stoppas in i Set *)
  type t = { key:key; data:D.elt }
  let create key data = { key; data }
  let get_key { key } = key
  let get_data { data } = data
  (* testar två t med varandar, används av Set (här kollar vi bara nyckeln key) *)
  let compare {key=key1} {key=key2} = Pervasives.compare key1 key2
end

(* Skapar en modul där typen är en path *)
module PathData = Data(struct type elt = Path.t end)

(* 
 * Lagrar PathData.t (som är en path) och string som nyckel.
 *
 * Nyckeln kommer vara värdet från Path.hash_cycle.
 *
 * Cykel mellan samma noder genererar samma hash-värde.
 *
 * Modulen DataSet ignorerar dubletter av samma hash
 * och kan alltså användas för att filtrera bort dubletter
 * av samma cykel.
 *)
module DataSet = Set.Make(PathData)
