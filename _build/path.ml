(* Modulen Path, fil path.ml *)

(*
 * Denna modul används för att representera vandringar i en graf. En vandring kan antingen vara
 * en väg (trail), en stig (path) eller en cykel (cycle).
 * 
 * Dessa definieras enligt:
 *
 * Väg - Vandring som inte passerar samma kant mer än en gång.
 * Stig - En väg som inte passerar samma hörn mer än en gång.
 * Cykel - En stig som börjar och slutar i samma hörn.
 * 
 * 2019-06-09 Max Jonsson
 *)

module Path : sig
  type t = Path of (Node.t * Node.t * Node.t list)

  val first : t -> Node.t
  val last : t -> Node.t
  val nodes : t -> Node.t list

  val make : Node.t -> t
  val length : t -> int

  (* Förlänger en vandring med ett steg. Returnerar en lista med alla möjliga utfall *)
  val expand : t -> Node.t list -> t list

  (* Hash-koden används för att jämföra om två cykler representerar samma cykel *)
  val hash_cycle : t -> string
  
end = struct
  type t = Path of (Node.t * Node.t * Node.t list)

  let make node = Path (node, node, [node])

  let make2 first last lst = Path (first, last, lst)

  let rec find name lst =
    match lst with
    | [] -> None
    | h::t -> if name = Node.name h then Some h else find name t

  let nodes path =
    match path with
    | Path (first, last, lst) -> lst

  let first path =
    match path with
    | Path (first, last, lst) -> first

  let last path =
    match path with
    | Path (first, last, lst) -> last

  let length path = nodes path |> List.length

  let add node path =
    match path with
    | Path (first, last, lst) -> make2 first node (node :: lst)

  let expand path graph_lst =
    let rec aux lst acc =
      match lst with
      | [] -> acc
      | h::t -> aux t ((add h path) :: acc)
    in
    let last = last path in
    let node_opt = find (Node.name last) graph_lst in
    let neighbours = match node_opt with
      | None -> []
      | Some node -> Node.neighbours node in
    aux neighbours []

  let print_path path =
    let rec aux l acc =
      match l with
      | [] -> acc
      | h::t -> acc ^ h |> aux t
    in
    let lst = List.map Node.name (nodes path) in
    aux lst ""

  
  (* Specifika funktioner för cykel *)

  let remove_duplicates lst =
    let rec aux lst acc =
      match lst with
      | [] -> acc
      | h::t -> if List.exists (fun a -> h = a) acc then aux t acc else acc @ [h] |> aux t
    in
    aux lst []

  let rotate lst =
    let rec aux l key =
      match l with
      | [] as l -> l
      | h::t as l when h = key -> l
      | h::t -> aux (t @ [h]) key
    in
    match lst with
    | [] as lst -> lst
    | h::t as lst ->
      let lst = remove_duplicates lst in
      let smallest = List.fold_right (fun s acc -> if compare s acc < 0 then s else acc) t h in
      aux lst smallest

  let hash_cycle cycle =
    let rec aux l acc =
      match l with
      | [] -> acc
      | h::t -> acc ^ h |> aux t
    in
    let lst = List.map (fun node -> Node.name node) (nodes cycle) in
    aux (rotate (List.rev lst)) ""


end

include Path
