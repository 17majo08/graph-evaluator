(* Module Switches, file switches.ml *)

(*
 * Denna modul används för att köra de switchar som blivit satta av användaren i modulen Main.
 * Varje switch motsvarar en funktion i denna modul, som kommer att köras om de satts till true.
 *
 * 2019-06-09 Max Jonsson
 *)

module Switches : sig
  val nodes : Graph.t -> unit
  val edges : Graph.t -> unit
    
  val degrees : Graph.t -> unit
    
  val print_graph : Graph.t -> unit

  val num_trails : Graph.t -> unit
  val num_paths : Graph.t -> unit
  val num_cycles : Graph.t -> unit

  val min_trail : Graph.t -> unit
  val min_path : Graph.t -> unit
  val min_cycle : Graph.t -> unit

  val max_trail : Graph.t -> unit
  val max_path : Graph.t -> unit
  val max_cycle : Graph.t -> unit
  
end = struct

  let nodes graf =
    let nodes = Graph.num_of_nodes graf in
    Printf.(printf "Number of nodes %d\n" nodes);
    Printf.(printf "\n")

  let edges graf =
    let edges = Graph.num_of_edges graf in
    Printf.(printf "Number of edges %d\n" edges);
    Printf.(printf "\n")

  let degrees graf =
    let node_degrees = Graph.node_degree_out graf in
    List.iter (
      fun (name, degree) -> Printf.(printf "Node %s has the out degree %d\n" name degree)
    ) node_degrees;
    Printf.(printf "\n")

  let print_graph graf =
    Graph.print_graph graf;
    Printf.(printf "\n")

  let num_trails graf =
    let num_trails = Graph.num_trails graf in
    Printf.(printf "Number of trails %d\n" num_trails);
    Printf.(printf "\n")

  let num_paths graf =
    let num_paths = Graph.num_paths graf in
    Printf.(printf "Number of paths %d\n" num_paths);
    Printf.(printf "\n")

  let num_cycles graf =
    let num_cycles = Graph.num_cycles graf in
    Printf.(printf "Number of cycles %d\n" num_cycles);
    Printf.(printf "\n")

  let min_trail graf =
    let min_trail = Graph.min_trail graf in
    Printf.(printf "Length min trail %d\n" ((List.length min_trail) - 1));
    Printf.(printf "\n")

  let min_path graf =
    let min_path = Graph.min_path graf in
    Printf.(printf "Length min path %d\n" ((List.length min_path) - 1));
    Printf.(printf "\n")

  let min_cycle graf =
    let min_cycle = Graph.min_cycle graf in
    Printf.(printf "Length min cycle %d\n" ((List.length min_cycle) - 1));
    Printf.(printf "\n")

  let max_trail graf =
    let max_trail = Graph.max_trail graf in
    Printf.(printf "Length max trail %d\n" ((List.length max_trail) - 1));
    Printf.(printf "\n")

  let max_path graf =
    let max_path = Graph.max_path graf in
    Printf.(printf "Length max path %d\n" ((List.length max_path) - 1));
    Printf.(printf "\n")

  let max_cycle graf =
    let max_cycle = Graph.max_cycle graf in
    Printf.(printf "Length max cycle %d\n" ((List.length max_cycle) - 1));
    Printf.(printf "\n")

end

include Switches
