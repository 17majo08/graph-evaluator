(* Modulen Edge, fil edge.ml *)

(*
 * Denna modul används för att representera en kant mellan två noder.
 *
 * 2019-06-09 Max Jonsson
 *)

module Edge : sig
  type t = Edge of (Node.t * Node.t)

  val make : string -> string -> t

  val get_start : t -> Node.t
  val get_end : t -> Node.t

end = struct
  type t = Edge of (Node.t * Node.t)

  let make a b = Edge (Node.make a, Node.make b)

  let get_start edge = match edge with
    | Edge (start, _) -> start

  let get_end edge = match edge with
    | Edge (_, stop) -> stop

end

include Edge
